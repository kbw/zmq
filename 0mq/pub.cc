#include <0mqconf/config.hpp>
#include <0mqutil/strutil.hpp>
#include <0mqutil/procutil.hpp>
#include <unistd.h>

int main(int argc, char* argv[])
try
{
	zmq::context_t ctx(1);
	zmq::socket_t s{ config(ctx, "v2/0mq/pub") };

	if (argc > 1)
		create_peer(to_pchars(argc - 1, &argv[1]));

	std::string line;
	while (std::getline(std::cin, line) && !line.empty())
	{
		usleep(10*1000);	// 10ms
		s.send(to_msg(line));
	}

	sleep(1);
	s.send(zmq::message_t("ctrl:DONE", 9));
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "fatal: " << "unknown error" << std::endl;
}
