#include <0mqconf/config.hpp>
#include <0mqutil/strutil.hpp>
#include <0mqutil/procutil.hpp>
#include <unistd.h>

int main(int argc, char* argv[])
try
{
	if (argc < 2)
		return 1;

	zmq::context_t ctx(1);
	zmq::socket_t s{ config(ctx, "v2/0mq/push") };

	if (argc > 2)
		create_peer(to_pchars(argc - 2, &argv[2]));

	std::string line;
	while (std::getline(std::cin, line) && !line.empty())
		s.send(to_msg(line));

	sleep(1);
	s.send(zmq::message_t("ctrl:DONE", 9));
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "fatal: " << "unknown error" << std::endl;
}
