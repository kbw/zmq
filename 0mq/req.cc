#include <0mqconf/config.hpp>
#include <0mqutil/strutil.hpp>
#include <0mqutil/procutil.hpp>

int main(int argc, char* argv[])
try
{
	zmq::context_t ctx(1);
	zmq::socket_t s{ config(ctx, "v2/0mq/req") };

	if (argc > 1)
		create_peer(to_pchars(argc - 1, &argv[1]));

	std::string line;
	while (std::getline(std::cin, line))
	{
		s.send(to_msg(line));

		zmq::message_t msg{ recv(s) };
		std::cout << to_string(msg) << std::endl;
	}
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "fatal: " << "unknown error" << std::endl;
}
