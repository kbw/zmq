#include "riemann-sum.hpp"
#include <0mqutil/strutil.hpp>
#include <0mqutil/options.hpp>
#include <unistd.h>
#include <zmq.hpp>
#include <sstream>
#include <iomanip>		// setprecision, setw
#include <algorithm>	// generate
#include <numeric>		// accumulate
#include <stdlib.h>		// atoi

void worker(int portin, int portout, int id, int n_tasks, int n_workers);

int main(int argc, char* argv[])
{
	if (opts.verbose == 2)
		printf("worker:%d: argc=%d\n", getpid(), argc);
	if (argc != 6)
		return 1;
	if (opts.verbose == 2)
		printf("worker:%d: %s %s %s %s\n", getpid(), argv[1], argv[2], argv[3], argv[4]);
	worker(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]), atoi(argv[5]));
}

void connect(zmq::socket_t& in, zmq::socket_t& out, int port_in, int port_out);
void do_work(zmq::socket_t& in, zmq::socket_t& out, int id, int n_tasks, int n_workers);

void worker(int port_in, int port_out, int id, int n_tasks, int n_workers)
{
	zmq::context_t ctx(1);
	zmq::socket_t in(ctx, zmq::socket_type::pull);
	zmq::socket_t out(ctx, zmq::socket_type::push);

	connect(in, out, port_in, port_out);
	do_work(in, out, id, n_tasks, n_workers);
}

void connect(zmq::socket_t& in, zmq::socket_t& out, int port_in, int port_out)
{
	string in_addr = fmt("tcp://localhost:%d", port_in);
	string out_addr = fmt("tcp://localhost:%d", port_out);
	if (opts.verbose == 1)
		printf("worker:%d: in=%s out=%s\n", getpid(), in_addr.c_str(), out_addr.c_str());

	in.connect(in_addr);
	out.connect(out_addr);
}

namespace
{
	std::string to_string(const real_t& sum)
	{
		using namespace std;

		ostringstream os;
		os << setprecision(80) << setw(48) << fixed << sum;
		return os.str();
	}

	void send(zmq::socket_t& s, const std::string& str)
	{
		zmq::message_t msg(str.size());
		memcpy(msg.data(), str.c_str(), msg.size());
		s.send(msg);
	}
}

void do_work(zmq::socket_t& in, zmq::socket_t& out, int id, int n_tasks, int n_workers)
{
	{ zmq::message_t msg; in.recv(&msg); }	// wait for start

	real_t sum;
	{
		struct Riemann
		{
			int i;
			const int n;
			const real_t range;
			const real_t dx;

			Riemann(int n, real_t range, real_t dx) : i(0), n(n), range(range), dx(dx)
			{
			}

			real_t operator()()
			{
				real_t x = range + i++*dx;
				return rect(x, dx, f);
			}
		};

		const real_t range	= real_t(id)/n_workers;
		const real_t dx		= real_t(1)/(n_tasks*n_workers);
		std::vector<real_t> areas(n_tasks);
		std::generate(areas.begin(), areas.end(), Riemann(n_tasks, range, dx));
		sum = std::accumulate(areas.begin(), areas.end(), real_t());
	}

	const std::string str(to_string(sum));
	send(out, str);
	printf("id=%d rect=%s\n", id, str.c_str());
}
