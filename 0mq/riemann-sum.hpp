#pragma once

// work to be done:
//	Find the Riemann Sum of: y = x*x where x:[0, 1)
//	We use high precission floating point math
//	Each worker has n tasks, n workers
#include <mpfr_real/real.hpp>

typedef mpfr::real<8192, MPFR_RNDZ> real_t;
typedef real_t func_t(real_t x);

inline real_t f(real_t x)
{
	return x*x;
}

inline real_t rect(real_t x, real_t dx, func_t* f)
{
	real_t lh = abs(f(x));
	real_t rh = abs(f(x + dx));
	return lh*dx;
}
