#include <0mqconf/config.hpp>
#include <0mqutil/strutil.hpp>

int main(int argc, char* argv[])
try
{
	zmq::context_t ctx(1);
	zmq::socket_t s{ config(ctx, "v2/0mq/sub") };

	zmq::message_t imsg;
	while (s.recv(&imsg))
	{
		std::string s { to_string(imsg) };
		if (s == "ctrl:DONE")
			break;

		std::cout << s << std::endl;
	}
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "fatal: " << "unknown error" << std::endl;
}
