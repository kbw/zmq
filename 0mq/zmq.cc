#include <0mqutil/strutil.hpp>
#include <0mqutil/procutil.hpp>
#include <string.h>

bool known_app(const char* cmd);

int main(int argc, char*argv[])
{
	if (argc == 1)
		return 0;

	if (!known_app(argv[1]))
		return 2;

	return spawn(to_pchars(argc - 1, &argv[1]));
}

bool known_app(const char* cmd)
{
	static const char* const apps[] =
	{ "req", "rep", "pub", "sub", "push", "pull", nullptr };

	bool match = false;
	for (size_t i = 0; !match && apps[i]; ++i)
	{
		const char* const app = apps[i];

		const char* q = strrchr(cmd, '/');
		match = q
				? strcmp(app, q + 1) == 0
				: strcmp(app, cmd) == 0;
	}

	return match;
}
