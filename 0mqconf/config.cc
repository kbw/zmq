#include "config.hpp"
#include "db.hpp"
#include "map.hpp"
#include <0mqutil/strutil.hpp>	// fmt
#include <map>
#include <memory>
#include <algorithm>
#include <stdexcept>
#include <thread>
#include <stdlib.h>		// getenv

namespace
{
	const char* ZCONF = "ZCONF";
	const std::string ZCONFIG_PROTOCOL = "0config://";

	typedef std::multimap<string, string> kvs_t;
	typedef kvs_t::value_type kv_t;

	kvs_t parse(const char* start, int skip = 0)
	{
		auto next = [](const char* begin) -> const char*
		{
			std::clog << "e: " << string(begin) << std::endl;

			const char*p = strchr(begin, '/');
			if (!p)
				return begin;
			if (p[1] == '/')
				p = strchr(p + 2, '/');
			if (!p)
				return begin;

			std::clog << "x: " << string(begin, p) << std::endl;
			return p;
		};

		std::clog << "start: " << string(start) << std::endl;
		kvs_t coll;
		string key, val;
		const char *begin, *end = start - 1;
		for (int i = 0; i < skip; ++i)
		{
			begin = end + 1;
			end = next(begin);
		}

		while (true)
		{
			begin = end + 1;
			end = next(begin);
			if (begin == end)
				return coll;
			key.assign(begin, end);

			begin = end + 1;
			end = next(begin);
			if (begin == end)
				return coll;
			val.assign(begin, end);

			coll.insert(std::make_pair(key, val));
		}

		return coll;
	}

	const string& find(const kvs_t& params, const string& key)
	{
		kvs_t::const_iterator p = params.find(key);
		if (p != params.end())
			return p->second;

		static const string empty_str;
		return empty_str;
	}

	zmq::socket_t create(zmq::context_t& ctx, const string& zmqtype, const string& bindtype, const string& address)
	{
		std::clog
			<< fmt("create(%s, %s, %s)", zmqtype.c_str(), bindtype.c_str(), address.c_str())
			<< std::endl;

		zmq::socket_t s(ctx, to_type(zmqtype));

		if (zmqtype == "sub")
			s.setsockopt(ZMQ_SUBSCRIBE, "", 0); // request everything

		if (bindtype == "true")
			s.bind(address);
		else if (bindtype == "false")
			s.connect(address);
		else
			throw std::runtime_error("bad: connection spec: " + bindtype);

		return s;
	}
}

// Get Config Server address
const char* get_server_address()
{
	// get server address
	const char* srvaddr = getenv(ZCONF);
	wbassert(
		srvaddr,
		std::runtime_error("missing: environment variable ZCONF"));
	std::clog << "srvaddr: " << srvaddr << std::endl;

	return srvaddr;
}

zmq::socket_t create(zmq::context_t& ctx, const string& spec)
{
	// talk to server
	auto exec = [](zmq::context_t& ctx, const string& addr, const string& res) -> string
	{
		zmq::socket_t s(ctx, zmq::socket_type::req);
		s.connect(addr);
		s.send(to_msg(res));
		return to_string(recv(s));
	};

	const char* srvaddr = get_server_address();
	const std::string reply = exec(ctx, srvaddr, spec);
	std::clog << "reply: " << reply << std::endl;

	// validate/extract content
	wbassert(
		!strncmp(ZCONFIG_PROTOCOL.c_str(), reply.c_str(), ZCONFIG_PROTOCOL.size()),
		std::runtime_error("bad config protocol: " + reply));
	const char* content = reply.c_str() + ZCONFIG_PROTOCOL.size();

	kvs_t params = parse(content);
	const string& zmqtype = find(params, "zmqtype");
	const string& bindtype= find(params, "bindtype");
	const string& address = find(params, "address");

	// create socket
	return create(ctx, zmqtype, bindtype, address);
}

void heartbeat(zmq::context_t* ctx, const std::string& token, const std::string& spec)
try
{
	zmq::socket_t s = create(*ctx, spec);

	const string msg = "hbeat: " + token;

	for (;;sleep(10))
		s.send(to_msg(msg));
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "fatal: " << "unknown error" << std::endl;
}

// Config client -- create socket from token
zmq::socket_t config(zmq::context_t& ctx, const string& token, const char*)
{
	string conn = ZCONFIG_PROTOCOL + token + "/connect";
	string hbeat = ZCONFIG_PROTOCOL + token + "/heartbeat";

	std::thread* hbthread = new std::thread(heartbeat, &ctx, token, hbeat);
	hbthread->detach();

	return create(ctx, conn);
}

// Config server -- create database
namespace
{
	std::unique_ptr<zmqcat::config::db> local_db;
}

void dbinit(const string& instance)
{
	local_db.reset( zmqcat::config::map::create(instance) );
}

bool dblookup(const string& sys, const string& app, const string& serv, const string& key, string& val)
{
	return local_db->lookup(zmqcat::config::servname(sys, app, serv), key, val);
}

bool dblookup(const string& sys, const string& app, const string& serv, stringpairs& vals)
{
	bool ret = local_db->lookup(zmqcat::config::servname(sys, app, serv), vals);

	std::clog << "dblookup: name=" << sys << "/" << app << "/" << serv << std::endl;
	for (const auto& val : vals)
		std::clog << "\t" << val.first << "/" << val.second << std::endl;
	return ret;
}

// Config server -- parse incoming string into key/info pair
bool parse(const string& str, string& sys, string& app, string& serv, stringpairs& info)
{
	std::clog << "recv: " << str << std::endl;

	// validate
	wbassert(
		!strncmp(ZCONFIG_PROTOCOL.c_str(), str.c_str(), ZCONFIG_PROTOCOL.size()),
		std::runtime_error("bad config protocol: " + str));

	// find start of resources
	const char* content = str.c_str() + ZCONFIG_PROTOCOL.size();
	const char* p;

	p = strchr(content, '/');
	if (!p || !(p + 1))
	{
		sys = content;
		return false;	// no overrides
	}
	sys.assign(content, p);
	content = p + 1;

	p = strchr(content, '/');
	if (!p || !(p + 1))
	{
		app = content;
		return false;	// no overrides
	}
	app.assign(content, p);
	content = p + 1;

	p = strchr(content, '/');
	if (!p || !(p + 1))
	{
		serv = content;
		return false;	// no overrides
	}
	serv.assign(content, p);
	content = p + 1;

	return false;	// no overrides
}

// Config server -- apply overrides from one node to another
bool apply(stringpairs& dst, const stringpairs& src)
{
	for (stringpairs::const_iterator p = src.begin(); p != src.end(); ++p)
	{
		stringpairs::iterator q;
		for (q = dst.begin(); q != dst.end(); ++q)
			if (p->first == q->first)
				break;

		if (q != dst.end())
			q->second = p->second;
		else
			dst.push_back(*p);
	}

	return !src.empty();
}

// Config server -- construct a reply string
string construct(const stringpairs& node)
{
	string s = ZCONFIG_PROTOCOL;

	for (stringpairs::const_iterator p = node.begin(); p != node.end(); ++p)
		s += p->first + "/" + p->second + "/";

	std::clog << "send: " << s << std::endl;
	return s;
}
