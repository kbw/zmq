#pragma once

#include <0mqutil/defs.hpp>
#include <zmq.hpp>

// common
const char*	get_server_address();

// client side
zmq::socket_t 	config(
					zmq::context_t& ctx,
					const string& token,
					const char* addr = nullptr);

// server side
void	dbinit(const string& instance);
bool	dblookup(const string& sys, const string& app, const string& serv, const string& key, string& val);
bool	dblookup(const string& sys, const string& app, const string& serv, stringpairs& vals);

bool	parse(const string& str, string& sys, string& app, string& serv, stringpairs& info);
bool 	apply(stringpairs& dst, const stringpairs& src);
string	construct(const stringpairs& node);
