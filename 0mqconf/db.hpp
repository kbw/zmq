#pragma once

#include <0mqutil/defs.hpp>	// basic types
#include <map>

namespace zmqcat
{
	namespace config
	{
		typedef std::map<string, stringpairs> serv_t;
		typedef std::map<string, serv_t> app_t;
		typedef std::map<string, app_t> sys_t;

		class sysname
		{
		public:
			sysname(const string& name) : name(name) {}
			sysname() {}
			~sysname(){}

			const string& get_sysname() const	{ return name; }
			void set_sysname(const string& n)	{ name = n; }

		private:
			string	name;
		};

		class appname : public sysname
		{
		public:
			appname(const string& sys, const string& name) : sysname(sys), name(name) {}
			appname() {}
			~appname(){}

			const string& get_appname() const	{ return name; }
			void set_appname(const string& n)	{ name = n; }

		private:
			string	name;
		};

		class servname : public appname
		{
		public:
			servname(const string& sys, const string& app, const string& name) : appname(sys, app), name(name) {}
			servname() {}
			~servname(){}

			const string& get_servname() const	{ return name; }
			void set_servname(const string& n)	{ name = n; }

		private:
			string	name;
		};

		class db
		{
		public:
			virtual	~db() = 0;

			virtual	bool lookup(
							const servname& name,
							const string& key, string& value) const = 0;
			virtual	bool lookup(
							const servname& name,
							stringpairs& values) const = 0;
		};
	}
}
