// map.cc
#include "db.hpp"
#include "map.hpp"
#include <0mqutil/strutil.hpp>
#include <rapidjson/document.h>
#include <rapidjson/error/error.h>
#include <rapidjson/error/en.h>
#include <fstream>

namespace
{
/*
	const char* kTypeNames[] = 
    { "Null", "False", "True", "Object", "Array", "String", "Number" };
 */

	class context_t
	{
	public:
		typedef std::multimap<string, stringpairs> type_t;
		typedef std::multimap<string, type_t> types_t;

		context_t(zmqcat::config::sys_t& nodes) : nodes(nodes) {}

		const types_t&	types() const	{ return dictionary; }
		bool			processing_default() const;
		bool			processing_dictionary() const;

		const string&	text() const	{ return name.at(level() - 1); }
		size_t			level() const	{ return name.size(); }

		void			insert_dictionary(const char* key, const char* val);
		void 			insert(const char* key, const char* val);

		class lock_t
		{
		public:
			lock_t(context_t& ctx, const char* str) : ctx(ctx) { ctx.name.emplace_back(str); }
			~lock_t() { ctx.name.pop_back(); }

		private:
			context_t& ctx;
		};
		friend class lock_t;

	protected:
		stringpairs&		create_type(const stringpair& entry);
		const stringpairs&	find_type(const stringpair& entry) const;
		
	private:
		strings name;
		types_t	dictionary;
		zmqcat::config::sys_t&  nodes;
	};

	inline
	stringpairs& context_t::create_type(const stringpair& entry)
	{
		wbassert(
			level() == 3,
			std::runtime_error(fmt("JSON parser: size=%lu", level())));

		// identify key.1
		types_t::iterator p = dictionary.find(entry.first);
		if (p == dictionary.end())
			p = dictionary.emplace(std::make_pair(entry.first, type_t()));

		// identify key.2
		type_t::iterator q = p->second.find(entry.second);
		if (q == p->second.end())
			q = p->second.emplace(std::make_pair(entry.second, stringpairs()));

		// return list
		return q->second;
	}

	inline
	const stringpairs& context_t::find_type(const stringpair& entry) const
	{
		static const stringpairs empty;

		// identify key.1
		types_t::const_iterator p = dictionary.find(entry.first);
		if (p == dictionary.end())
			return empty;

		// identify key.2
		type_t::const_iterator q = p->second.find(entry.second);
		if (q == p->second.end())
			return empty;

		// return list
		return q->second;
	}

	inline
	void context_t::insert_dictionary(const char* key, const char* val)
	{
		std::clog
			<< "dict: add " << key << "/" << val
			<< " to " << name.at(1) << "/" << name.at(2)
			<< std::endl;

		stringpairs& pairs = create_type(std::make_pair(name.at(1), name.at(2)));
		pairs.emplace_back(std::make_pair(key, val));
	}

	void context_t::insert(const char* key, const char* val)
	{
		auto insert = [this](const stringpair& entry) -> void
		{
			// identify the subsystem
			zmqcat::config::sys_t::iterator p = nodes.find(name.at(0));
			if (p == nodes.end())
				p = nodes.emplace(std::make_pair(name[0], zmqcat::config::app_t())).first;

			// identify the section
			zmqcat::config::app_t::iterator q = p->second.find(name.at(1));
			if (q == p->second.end())
				q = p->second.emplace(std::make_pair(name[1], zmqcat::config::serv_t())).first;

			// identify component within subsystem
			zmqcat::config::serv_t::iterator r = q->second.find(name.at(2));
			if (r == q->second.end())
				r = q->second.emplace(std::make_pair(name[2], stringpairs())).first;

			// prform a linear search thru the list of pairs
			for (stringpairs::iterator it = r->second.begin(); it != r->second.end(); ++it)
				if (it->first == entry.first)
				{
					it->second = entry.second;
					return;
				}

			// finally append the entry to the component's list
			r->second.emplace_back(entry);
		};

		const stringpair entry = std::make_pair(key, val);

		// do key/val describe a dictionary entry?
		const stringpairs& pairs = find_type(entry);
		if (!pairs.empty())
		{
			// YES: instead of inserting key/val, add the set of pairs
			for (stringpairs::const_iterator p = pairs.begin(); p != pairs.end(); ++p)
				insert(*p);
		}
		else
			insert(entry);
	}

	inline
	bool context_t::processing_default() const
	{
		return level() > 0 && name[0] == "default";
	}

	inline
	bool context_t::processing_dictionary() const
	{
		return processing_default() && level() > 1;
	}

	void parse(
		context_t& ctx, size_t level,
		rapidjson::Value::ConstMemberIterator begin,
		rapidjson::Value::ConstMemberIterator end)
	{
		for (rapidjson::Value::ConstMemberIterator p = begin; p != end; ++p)
		{
			if (p->name.IsString() && p->value.IsObject())
			{
//				wbassert(
//					level == ctx.level(),
//					std::runtime_error(
//						fmt("JSON parser: level=%lu, size=%lu, name=%s",
//							level, ctx.level(), p->name.GetString())));
				context_t::lock_t lock(ctx, p->name.GetString());

				std::clog << level << ":" << p->name.GetString() << std::endl;
				parse(ctx, level + 1, p->value.MemberBegin(), p->value.MemberEnd());
			}
			else if (ctx.processing_dictionary())
			{
				if (p->name.IsString() && p->value.IsString())
				{
//					wbassert(
//						level == 3,
//						std::runtime_error(
//							fmt("JSON parser: level=%lu, size=%lu, name=%s value=%s",
//								level, ctx.level(), p->name.GetString(), p->value.GetString())));
//
					ctx.insert_dictionary(p->name.GetString(), p->value.GetString());
				}
				else if (p->name.IsString() && p->value.IsBool())
				{
//					wbassert(
//						level == 3,
//						std::runtime_error(
//							fmt("JSON parser: level=%lu, size=%lu, name=%s value=%s",
//								level, ctx.level(), p->name.GetString(), p->value.GetString() ? "true" : "false")));
//
					ctx.insert_dictionary(p->name.GetString(), p->value.GetBool() ? "true" : "false");
				}
			}
			else
			{
				if (p->name.IsString() && p->value.IsString())
				{
					ctx.insert(p->name.GetString(), p->value.GetString());
				}
				else if (p->name.IsString() && p->value.IsBool())
				{
					ctx.insert(p->name.GetString(), p->value.GetBool() ? "true" : "false");
				}
			}
		}
	}

	void parse(
		zmqcat::config::sys_t& nodes,
		rapidjson::Value::ConstMemberIterator begin,
		rapidjson::Value::ConstMemberIterator end)
	{
		context_t ctx(nodes);
		parse(ctx, 0, begin, end);

		for (auto p = ctx.types().begin(); p != ctx.types().end(); ++p)
		{
			std::clog << p->first << std::endl;
			for (auto q = p->second.begin(); q != p->second.end(); ++q)
			{
				std::clog << "\t" << q->first << std::endl;
				for (auto r = q->second.begin(); r != q->second.end(); ++r)
					std::clog << "\t\t" << r->first << ":" << r->second << std::endl;
			}
		}

		for (auto p = nodes.begin(); p != nodes.end(); ++p)
		{
			std::clog << p->first << std::endl;
			for (auto q = p->second.begin(); q != p->second.end(); ++q)
			{
				std::clog << "\t" << q->first << std::endl;
				for (auto r = q->second.begin(); r != q->second.end(); ++r)
				{
					std::clog << "\t\t" << r->first << std::endl;
					for (auto it = r->second.begin(); it != r->second.end(); ++it)
						std::clog << "\t\t\t" << it->first << ":" << it->second << std::endl;
				}
			}
		}
	}

	zmqcat::config::sys_t init(const string& instance)
	{
		zmqcat::config::sys_t nodes;

		auto read = [](const string& filename) -> string
		{
			string str, line;
			std::ifstream is(filename);
			while (std::getline(is, line))
				str += line;
			return str;
		};
		const string filename(instance + ".json");
		const string json = read(filename);
		std::clog << "json: " << json << std::endl;

		rapidjson::Document doc;
		doc.Parse(json.c_str());
		wbassert(
			!doc.HasParseError(),
			std::runtime_error(
				fmt("JSON parse error: %s:%u",
					rapidjson::GetParseError_En(doc.GetParseError()),
					(unsigned)doc.GetErrorOffset())));

		if (doc.IsObject())
		{
			// format is:
			//	name
			//		type:{ <node> } [repeated]
			parse(nodes, doc.MemberBegin(), doc.MemberEnd());
		}

		return nodes;
	}
}

namespace zmqcat
{
	namespace config
	{
		map::map(const string& instance) :
			nodes(init(instance))
		{
		}

		bool map::lookup(const servname& name, const string& key, string& value) const
		{
			sys_t::const_iterator p = nodes.find(name.get_sysname());
			if (p != nodes.end())
			{
				app_t::const_iterator q = p->second.find(name.get_appname());
				if (q != p->second.end())
				{
					serv_t::const_iterator r = q->second.find(name.get_servname());
					if (r != q->second.end())
						for (stringpairs::const_iterator it = r->second.begin(); it != r->second.end(); ++it)
							if (it->first == key)
							{
								value = it->second;
								return true;
							}
				}
			}

			return false;
		}

		bool map::lookup(const servname& name, stringpairs& values) const
		{
			sys_t::const_iterator p = nodes.find(name.get_sysname());
			if (p != nodes.end())
			{
				app_t::const_iterator q = p->second.find(name.get_appname());
				if (q != p->second.end())
				{
					serv_t::const_iterator r = q->second.find(name.get_servname());
					if (r != q->second.end())
						values = r->second;
				}
			}

			return !values.empty();
		}

		zmqcat::config::db* map::create(const string& instance)
		{
			return new map(instance);
		}
	}
}
