#pragma once

#include "db.hpp"

namespace zmqcat
{
	namespace config
	{
		class map : public db
		{
		public:
			bool lookup(
					const servname& name,
					const string& key, string& value) const;
			bool lookup(
					const servname& name,
					stringpairs& values) const;

			static db* create(const string& instance);

		private:
			map(const string& instance);

			sys_t nodes;
		};
	}
}
