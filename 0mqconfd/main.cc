#include <0mqconf/config.hpp>
#include <0mqutil/strutil.hpp>

int main(int argc, char* argv[])
try
{
	if (argc < 2)
		return 1;
	dbinit(argv[1]);

	zmq::context_t ctx(1);
	zmq::socket_t s(ctx, zmq::socket_type::rep);
	s.bind(get_server_address());

	while (true)
	{
		string req = to_string(recv(s));
		std::cout << req << std::endl;

		string sys, app, serv;
		stringpairs values, overrides;
		parse(req, sys, app, serv, overrides);	// parse request
		dblookup(sys, app, serv, values);		// lookup values
		apply(values, overrides);				// apply user specified overrides

		s.send(to_msg(construct(values)));
	}
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}
