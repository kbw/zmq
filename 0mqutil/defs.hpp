#pragma once

#include <vector>
#include <list>
#include <utility>
#include <string>
#include <stdexcept>
#include <iostream>

#define wbassert(cond, expr)	if (!(cond)) throw (expr);
#define wbalert(cond, expr)		if (!(cond)) std::clog << (expr).what() << std::endl;

typedef std::string					string;
typedef std::vector<string>			strings;
typedef std::vector<char*>			pchars;
typedef std::vector<char>			chars;
typedef std::pair<string, string>	stringpair;
typedef std::list<stringpair>		stringpairs;
