#include "procutil.hpp"
#include <unistd.h>		// pipe, dup, fork, exec, getcwd

void create_peer(pchars args)
{
	int in_fd[2], out_fd[2];
	pipe(in_fd), pipe(out_fd);

	int pid = fork();
	if (pid == 0)
	{
		// redirect thru swaped stdin/stdout
		dup2(out_fd[STDIN_FILENO], STDIN_FILENO);
		dup2(in_fd[STDOUT_FILENO], STDOUT_FILENO);

		// close pipes
		close(in_fd[STDIN_FILENO]); close(out_fd[STDOUT_FILENO]);
		close(in_fd[STDOUT_FILENO]); close(out_fd[STDIN_FILENO]);

		spawn(args);
	}

	// redirect thru pipe stdin/stdout
	dup2(in_fd[STDIN_FILENO], STDIN_FILENO);
	dup2(out_fd[STDOUT_FILENO], STDOUT_FILENO);

	// close pipes
	close(in_fd[STDIN_FILENO]); close(out_fd[STDOUT_FILENO]);
	close(in_fd[STDOUT_FILENO]); close(out_fd[STDIN_FILENO]);
}

int spawn(pchars args)
{
	static int path_patched;
	if (!path_patched)
	{
		chars pwd(64);
		while (!getcwd(pwd.data(), pwd.size()))
			pwd.resize(2*pwd.size());

		string path(pwd.data());
		path += ":./:";
		if (const char* p = getenv("PATH"))
			path += p;
		setenv("PATH", path.c_str(), 1);

		path_patched = 1;
	}

	if ( execvp(args.front(), &args.front()) )
		return errno;

	return 0;
}
