#include "strutil.hpp"
#include <map>
#include <stdexcept>
#include <stdarg.h>		// va_ ...
#include <stdio.h>		// asprintf

std::string fmt(const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	char* buf = nullptr;
	int bufsz = vasprintf(&buf, fmt, va);
	va_end(va);

	std::string s;
	if (buf && bufsz > 0)
		s.assign(buf, bufsz);
	free(buf);
	return s;
}

pchars to_pchars(int argc, char* argv[])
{
	pchars args;
	for (int i = 0; i < argc; ++i)
		args.push_back(argv[i]);
	args.push_back(nullptr);

	return args;
}

zmq::message_t recv(zmq::socket_t& s)
{
	zmq::message_t msg;
	s.recv(&msg);

	return msg;
}

zmq::message_t to_msg(const string& str)
{
	return zmq::message_t(str.c_str(), str.size());
}

std::string to_string(const zmq::message_t& msg)
{
	return std::string(
				reinterpret_cast<const char *>(msg.data()),
				msg.size());
}

zmq::socket_type to_type(const string& str)
{
	typedef std::map<string, zmq::socket_type> type_map_t;

	static const type_map_t map =
	{
		{ "pub", zmq::socket_type::pub },
		{ "sub", zmq::socket_type::sub },
		{ "req", zmq::socket_type::req },
		{ "rep", zmq::socket_type::rep },
		{ "push", zmq::socket_type::push },
		{ "pull", zmq::socket_type::pull }
	};

	type_map_t::const_iterator p = map.find(str);
	if (p == map.end())
		throw std::runtime_error(
				fmt("%s: \"%s\"",
					"unrecognized zmq::socket_type in to_type",
					str.c_str()));

	return p->second;
}
