#pragma once

#include "defs.hpp"
#include <zmq.hpp>

string fmt(const char* fmt, ...) __attribute__((format(printf, 1, 2)));
pchars to_pchars(int argc, char* argv[]);

zmq::message_t recv(zmq::socket_t& s);
zmq::message_t to_msg(const string& msg);
string to_string(const zmq::message_t& msg);
zmq::socket_type to_type(const string& type);
