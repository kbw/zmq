#pragma once

#include <unicode/unistr.h>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#define LOG_INFO	std::clog << "log: "
#define LOG_ERROR	std::cerr << "error: "

#define LOG_AND_THROW(X)	{ auto x = (X); LOG_ERROR << "Func:[" << __PRETTY_FUNCTION__ << "] Exception:[" << x.what() << "]\n"; throw x; }
#define LOG_NOTHROW(X)		{ auto x = (X); LOG_ERROR << "Func:[" << __PRETTY_FUNCTION__ << "] Exception:[" << x.what() << "]\n"; }

#define ASSERT(expr, action){ if ( !(expr) ) LOG_AND_THROW(action) }
#define ALERT(expr, action)	{ if ( !(expr) ) LOG_NOTHROW(action) }

template <typename D, typename S>
inline D convert_cast(S src)
{
	auto dst = static_cast<D>(src);
	if (static_cast<S>(dst) != src)
		throw std::runtime_error("convert_cast<> failed");

	return dst;
}

typedef std::string basic_string;
typedef icu::UnicodeString string;
typedef std::vector<string> strings;

template <>
inline std::string convert_cast<std::string>(string src)
{
	std::string dst;
	src.toUTF8String(dst);
	return dst;
}
