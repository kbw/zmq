#include "options.hpp"
#include <fstream>
#include <iostream>

void run(Options& opts);

int main(int argc, char* argv[])
{
	try
	{
		Options opts = std::for_each(argv + 1, argv + argc, Options());
		run(opts);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}

zmq::message_t to_msg(const basic_string& data);

void exec(zmq::socket_t& socket, std::istream& stream);
void exec(zmq::socket_t& socket, std::ostream& stream);
void exec(zmq::socket_t& socket, const basic_string& line);

void run(Options& opts)
{
	switch (opts.source_type)
	{
	case Options::source_t::data:
		switch (opts.dir_type)
		{
		case Options::dir_t::in:
			{
				std::ifstream is(opts.filename);
				exec(*opts.s.get(), is);
			}
			break;

		case Options::dir_t::out:
			{
				std::ofstream os(opts.filename);
				exec(*opts.s.get(), os);
			}
			break;

		case Options::dir_t::io:
			break;
		}
		break;

	case Options::source_t::file:
		switch (opts.dir_type)
		{
		case Options::dir_t::in:
			{
				LOG_INFO << "opening: " << opts.filename << "\n";
				std::ifstream is(opts.filename);
				basic_string line;
				while (std::getline(is, line))
				{
					LOG_INFO << "read: \"" << line << "\"\n";
					exec(*opts.s.get(), line);
				}
			}
			break;

		case Options::dir_t::out:
			exec(*opts.s.get(), opts.data);

		case Options::dir_t::io:
			break;
		}
		break;
	}
}

zmq::message_t to_msg(const basic_string& data)
{
	zmq::message_t msg(data.size());
	memcpy(msg.data(), data.c_str(), msg.size());
	return msg;
}

void exec(zmq::socket_t& socket, std::istream& stream)
{
	basic_string line;
	while (std::getline(stream, line))
	{
		zmq::message_t msg( to_msg(line) );
		socket.send(msg);
		socket.recv(&msg);
		std::cout << basic_string((const char*)msg.data(), msg.size()) << "\n";
	}
}

void exec(zmq::socket_t& socket, std::ostream& stream)
{
	try
	{
		zmq::message_t msg;
		while (socket.recv(&msg))
		{
			stream.write((const char*)msg.data(), msg.size());
		}
	}
	catch (const std::exception& e)
	{
		// client disconnected
	}
}

void exec(zmq::socket_t& socket, const basic_string& line)
{
	zmq::message_t msg( to_msg(line) );
	socket.send(msg);
}
