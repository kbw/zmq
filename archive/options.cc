#include "options.hpp"

//---------------------------------------------------------------------------

const Options::protocol_map_t Options::sm_protocol_map =
{
	{ "req",	{ zmq::socket_type::req,	dir_t::in }  },
	{ "rep",	{ zmq::socket_type::rep,	dir_t::out } },
	{ "delear",	{ zmq::socket_type::dealer,	dir_t::in }  },
	{ "router",	{ zmq::socket_type::router,	dir_t::out } },
	{ "pub",	{ zmq::socket_type::pub,	dir_t::out } },
	{ "sub",	{ zmq::socket_type::sub,	dir_t::in }  },
	{ "xpub",	{ zmq::socket_type::xpub, 	dir_t::out } },
	{ "xsub",	{ zmq::socket_type::xsub, 	dir_t::in }  },
	{ "push",	{ zmq::socket_type::push, 	dir_t::in }  },
	{ "pull",	{ zmq::socket_type::pull, 	dir_t::out } },
	{ "pair",	{ zmq::socket_type::pair,	dir_t::io }  },
	{ "stream",	{ zmq::socket_type::stream, dir_t::io }  }
};

zmq::socket_type Options::get_socket_type(basic_string s)
{
	auto p = get_protocols().find(s);
	if (p == get_protocols().end())
		throw std::runtime_error(convert_cast<basic_string>(s));

	return p->second.first;
}

Options::dir_t Options::get_direction_type(basic_string s)
{
	auto p = get_protocols().find(s);
	if (p == get_protocols().end())
		throw std::runtime_error(convert_cast<basic_string>(s));

	return p->second.second;
}

//---------------------------------------------------------------------------

const Options::connection_map_t Options::sm_connection_map =
{
	{ "bind",	connection_t::bind },
	{ "connect",connection_t::connect }
};

Options::connection_t Options::get_connection(basic_string s)
{
	auto p = get_connections().find(s);
	if (p == get_connections().end())
		throw std::runtime_error(convert_cast<basic_string>(s));

	return p->second;
}

//---------------------------------------------------------------------------

const Options::source_map_t Options::sm_source_map =
{
	{ "data",	source_t::data },
	{ "file",	source_t::file }
};

Options::source_t Options::get_source(basic_string s)
{
	auto p = get_sources().find(s);
	if (p == get_sources().end())
		throw std::runtime_error(convert_cast<basic_string>(s));

	return p->second;
}

//---------------------------------------------------------------------------

void Options::operator()(char* arg)
{
	if (!arg)
		return;

	if (strncasecmp(arg, "--", 2) == 0)
	{
		bool cont = false;

		switch (state)
		{
		case state_t::protocol:
			sock_type = get_socket_type(arg + 2);
			dir_type = get_direction_type(arg + 2);
			s.reset(new zmq::socket_t(*ctx.get(), sock_type));
			state = state_t::connection;
			break;

		case state_t::connection:
			conn_type = get_connection(arg + 2);
			state = state_t::address;
			break;

		case state_t::address:
			cont = true;	// drop out to the next switch
			break;

		case state_t::data:
			source_type = get_source(arg + 2);
			state = state_t::name;
			break;

		case state_t::name:
		case state_t::done:
			cont = true;	// drop out to the next switch
			break;
		}

		if (!cont)
			return;
	}

	switch (state)
	{
	case state_t::protocol:
	case state_t::connection:
		break;

	case state_t::address:
		state = state_t::data;
		addr = arg;
		switch (conn_type)
		{
		case connection_t::bind:
			s.get()->bind(addr.c_str());
			break;
		case connection_t::connect:
			s.get()->connect(addr.c_str());
			break;
		}
		break;

	case state_t::data:
		break;

	case state_t::name:
		state = state_t::done;
		switch (source_type)
		{
		case source_t::data:
			data = arg;
			LOG_INFO << "data=\"" << data << "\"\n";
			break;
		case source_t::file:
			filename = arg;
			LOG_INFO << "filename=\"" << filename << "\"\n";
			break;
		}
		break;

	case state_t::done:
		LOG_INFO << "done parsing\n";
		break;
	}
}
