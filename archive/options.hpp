#pragma once

#include "defs.hpp"
#include <zmq.hpp>
#include <map>
#include <memory>

struct Options
{
	// zmq::socket
	enum class dir_t { in, out, io };
	typedef std::pair<zmq::socket_type, dir_t> zprotocol_t;
	typedef std::map<basic_string, zprotocol_t> protocol_map_t;

	static const protocol_map_t 	sm_protocol_map;
	static zmq::socket_type			get_socket_type(basic_string s);
	static dir_t					get_direction_type(basic_string s);
	static const protocol_map_t&	get_protocols();

	// 0MQ connection type: REQ, REP, PUB, ...
	enum class connection_t { bind, connect };
	typedef std::map<basic_string, connection_t> connection_map_t;

	static const connection_map_t	sm_connection_map;
	static connection_t				get_connection(basic_string s);
	static const connection_map_t&	get_connections();

	// source/sink type
	enum class source_t { data, file };
	typedef std::map<basic_string, source_t> source_map_t;

	static const source_map_t		sm_source_map;
	static source_t					get_source(basic_string s);
	static const source_map_t&		get_sources();

	// Parse state
	enum class state_t { protocol, connection, address, data, name, done };
	state_t state;		// parse state

	zmq::socket_type sock_type;			// 0MQ connection type
	dir_t			dir_type;			// Input or Output
	connection_t	conn_type;			// bind or connect
	source_t		source_type;		// data or file
	std::shared_ptr<zmq::context_t> ctx;// 0MQ context
	std::shared_ptr<zmq::socket_t> s;	// 0MQ socket
	basic_string	addr;				// socket address
	basic_string	filename;			// i/o filename
	basic_string	data;				// data to send

	Options();
	~Options();

	void operator()(char* arg);
};

#include "options.inl"
