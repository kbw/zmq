
//---------------------------------------------------------------------------

inline Options::Options() :
	state(state_t::protocol),
	ctx(new zmq::context_t(1))
{
}

inline Options::~Options()
{
}

inline const Options::connection_map_t& Options::get_connections()
{
	return sm_connection_map;
}

inline const Options::protocol_map_t& Options::get_protocols()
{
	return sm_protocol_map;
}

inline const Options::source_map_t& Options::get_sources()
{
	return sm_source_map;
}
