#include "router_context.hpp"

/*
protocol_map_t router_context::sm_protocol_map =
{
	{ "REQ",	{ zmq::socket_type::req,	dir::in }  },
	{ "REP",	{ zmq::socket_type::rep,	dir::out } },
	{ "DEALER",	{ zmq::socket_type::dealer,	dir::in }  },
	{ "ROUTER",	{ zmq::socket_type::router,	dir::out } },
	{ "PUB",	{ zmq::socket_type::pub,	dir::out } },
	{ "SUB",	{ zmq::socket_type::sub,	dir::in }  },
	{ "XPUB",	{ zmq::socket_type::xpub, 	dir::out } },
	{ "XSUB",	{ zmq::socket_type::xsub, 	dir::in }  },
	{ "PUSH",	{ zmq::socket_type::push, 	dir::in }  },
	{ "PULL",	{ zmq::socket_type::pull, 	dir::out } },
	{ "PAIR",	{ zmq::socket_type::pair,	dir::io }  },
	{ "STREAM",	{ zmq::socket_type::stream, dir::io }  }
};
 */

router_context::router_context(int zmq_ctx_id) :
	m_ctx(zmq_ctx_id)
{
}

router_context::operator zmq::context_t& ()
{
	return m_ctx;
}

/*
const protocol_map_t& router_context::get_protocols()
{
	return sm_protocol_map;
}
 */
