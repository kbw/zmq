#pragma once

#include "defs.hpp"
#include "stream.hpp"
#include <zmq.hpp>
#include <memory>
#include <map>

/*
enum class dir { in, out, io };
typedef std::pair<zmq::socket_type, dir> zprotocol;
typedef std::map<string, zprotocol> protocol_map_t;
 */

struct router_context
{
public:
	router_context(int zmq_ctx_id = 1);
	operator zmq::context_t& ();

//	static const protocol_map_t& get_protocols();

private:
	zmq::context_t m_ctx;
	std::unique_ptr<stream>	m_stream;

//	static protocol_map_t sm_protocol_map;
};
