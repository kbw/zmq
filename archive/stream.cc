#include "stream.hpp"
#include <unistd.h>
#include <zmq.hpp>

stream::stream(int fd) :
	tag(etag::os)
{
	h.fd = fd;
}

stream::stream(zmq::socket_t* zconn) :
	tag(etag::zmq)
{
	h.zconn = zconn;
}

stream::~stream()
{
	switch (tag)
	{
	case etag::os:
		close(h.fd);
		break;

	case etag::zmq:
		delete h.zconn;
		break;

	default:
		;
	}
}
