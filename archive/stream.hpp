// an amalgamation of a Unix stream and 0mq stream
//
#pragma once

namespace zmq
{
	class socket_t;
}

class stream
{
public:
	stream(int fd);
	stream(zmq::socket_t* zconn);
	~stream();

private:
	enum class etag { os, zmq };

	union handle
	{
		int	fd;
		zmq::socket_t* zconn;
	};

	stream() = delete;
	stream(const stream &) = delete;
	stream& operator=(const stream &) = delete;

	etag tag;
	handle h;
};
