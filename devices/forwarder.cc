//#include <czmq_library.h>
#include <czmq++/czmqpp.hpp>
#include <unistd.h>		// sleep

#include <string>
#include <list>
#include <vector>
#include <thread>
#include <iostream>
#include <stdexcept>

#include <stdarg.h>		// va_ ...
#include <stdio.h>		// snprintf
#include <time.h>

typedef std::string string;
typedef	std::vector<string> strings;

std::string fmt(const char* fmt, ...);
czmqpp::message recv(czmqpp::socket& s, int flags = 0);
czmqpp::message to_msg(const string& str);
std::string to_string(const czmqpp::message& msg);

//---------------------------------------------------------------------------

namespace
{
	const strings inaddrs	{ "tcp://127.0.0.1:5557" }; 
	const string outaddr	{ "tcp://127.0.0.1:5558" };
}

int main()
try
{
	czmqpp::context ctx;

	czmqpp::socket frontend(ctx, ZMQ_SUB);	frontend.connect("tcp://*:5556");
	czmqpp::socket frontend2(ctx, ZMQ_SUB);	frontend2.connect("tcp://*:5557");
	czmqpp::socket backend(ctx, ZMQ_XPUB);	backend.bind("tcp://*:5558");
	zsocket_set_subscribe(frontend.self(), "");

	while (true)
	{
		zmq_pollitem_t items[] =
		{
			{ frontend.self(), 0, ZMQ_POLLIN, 0 },
			{ frontend2.self(),0, ZMQ_POLLIN, 0 },
			{ backend.self(),  0, ZMQ_POLLIN, 0 },
		};

		if (zmq_poll(items, 3, -1) == -1)
			break;

		if (items[0].revents & ZMQ_POLLIN)
		{
			char* topic = zstr_recv(frontend.self());
			char* connect = zstr_recv(frontend.self());
			if (!topic)
				break;

			zstr_send(backend.self(), topic);	zstr_free(&topic);
			zstr_send(backend.self(), connect);	zstr_free(&connect);
		}
		if (items[1].revents & ZMQ_POLLIN)
		{
			char* topic = zstr_recv(frontend2.self());
			char* connect = zstr_recv(frontend2.self());
			if (!topic)
				break;

			zstr_send(backend.self(), topic);	zstr_free(&topic);
			zstr_send(backend.self(), connect);	zstr_free(&connect);
		}
		if (items[2].revents & ZMQ_POLLIN)
		{			
		}
	}
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}

//---------------------------------------------------------------------------

std::string fmt(const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	char* buf = nullptr;
	int bufsz = vasprintf(&buf, fmt, va);
	va_end(va);

	std::string s;
	if (buf && bufsz > 0)
		s.assign(buf, bufsz);
	free(buf);
	return s;
}

/*
czmqpp::message recv(czmqpp::socket& s, int flags)
{
	czmqpp::message msg;
	s.recv(&msg, flags);

	return msg;
}

czmqpp::message to_msg(const string& str)
{
	return czmqpp::message(str.c_str(), str.size());
}

std::string to_string(const czmqpp::message& msg)
{
	return std::string(
				reinterpret_cast<const char *>(msg.data()),
				msg.size());
}
 */
