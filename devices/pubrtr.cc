#include <zmq.h>
#include <zmq.hpp>
#include <unistd.h>		// sleep

#include <string>
#include <list>
#include <vector>
#include <thread>
#include <iostream>
#include <stdexcept>

#include <stdarg.h>		// va_ ...
#include <stdio.h>		// snprintf
#include <time.h>

typedef std::string string;
typedef	std::vector<string> strings;

std::string fmt(const char* fmt, ...);
zmq::message_t recv(zmq::socket_t& s, int flags = 0);
zmq::message_t to_msg(const string& str);
std::string to_string(const zmq::message_t& msg);

//---------------------------------------------------------------------------

typedef std::unique_ptr<zmq::socket_t> psocket;
typedef std::pair<psocket, psocket> socketpair;
typedef std::vector<socketpair> socketpairs;
typedef std::list<std::thread> threads;

namespace
{
	void create_pubs(zmq::context_t& ctx, socketpairs& s, const strings& addr);
	void create_sub(zmq::context_t& ctx, socketpair& s, const string& addr);

	const strings inaddrs	{ "tcp://127.0.0.1:5561" }; 
//	const strings inaddrs	{ "tcp://127.0.0.1:5561", "tcp://127.0.0.1:5562" }; 
	const string outaddr	{ "tcp://127.0.0.1:5560" };
}

int main()
try
{
	zmq::context_t ctx(1);

	socketpairs in;
	socketpair out;
	create_pubs(std::ref(ctx), std::ref(in), std::cref(inaddrs));
	create_sub(std::ref(ctx), std::ref(out), std::cref(outaddr));

	// pass subscription up thru device
/*
	for (socketpairs::iterator p = in.begin(); p != in.end(); ++p)
	{
		zmq::socket_t& sub  = *out.first.get();
		zmq::socket_t& xpub = *out.second.get();
		zmq::socket_t& xsub = *p->second.get();

		std::clog << "priming device" << std::endl;
		xsub.send(recv(xpub, 10));
		sleep(1);
	}
 */
	in.begin()->second->send(recv(*out.second.get()));

	// pass messages down thru device
	for (socketpairs::iterator p = in.begin(); p != in.end(); ++p)
	{
		auto pump = [](void* xpub, void* xsub) { zmq_device(0, xpub, xsub); };

		std::clog << "starting device" << std::endl;
		void* xpub = static_cast<void*>(*p->second.get());
		void* xsub = static_cast<void*>(*out.second.get());

		std::thread* t = new std::thread(pump, xpub, xsub);
		t->detach();
	}

	threads tests;
	for (socketpairs::iterator p = in.begin(); p != in.end(); ++p)
	{
		auto test = [](zmq::socket_t& pub, zmq::socket_t& sub) -> void
		{
			// test run
			for (int i = 0; i != 5; ++i)
			{
				pub.send(to_msg(fmt("hello: %d", i)));
				std::clog << to_string(recv(sub)) << std::endl;
			}
		};
		zmq::socket_t& pub = *(p->first.get());
		zmq::socket_t& sub = *(out.first.get());

		std::clog << "starting test" << std::endl;
		tests.emplace_back(test, std::ref(pub), std::ref(sub));
	}
	for (threads::iterator p = tests.begin(); p != tests.end(); ++p)
	{
		std::clog << "waiting for test to complete" << std::endl;
		p->join();
	}
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}

namespace
{
	void create_pubs(zmq::context_t& ctx, socketpairs& s, const strings& addrs)
	{
		for (strings::const_iterator p = addrs.begin(); p != addrs.end(); ++p)
		{
			s.push_back(std::make_pair(
				psocket(new zmq::socket_t(ctx, ZMQ_PUB)),
				psocket(new zmq::socket_t(ctx, ZMQ_XSUB))));

			zmq::socket_t& pub = *(s.back().first.get());
			zmq::socket_t& xsub= *(s.back().second.get());
			const string& addr = *p;

			try
			{
				xsub.bind(addr);
				pub.connect(addr);
			}
			catch (const std::exception& e)
			{
				std::clog << "create_pubs connect error: addr: " << addr << std::endl;
				s.pop_back();
				continue;
			}
		}
	};

	void create_sub(zmq::context_t& ctx, socketpair& s, const string& addr)
	{
		s.first.reset(new zmq::socket_t(ctx, ZMQ_SUB));
		s.second.reset(new zmq::socket_t(ctx, ZMQ_XPUB));

		zmq::socket_t& sub = *(s.first.get());
		zmq::socket_t& xpub = *(s.second.get());

		try
		{
			xpub.bind(addr);
			sub.connect(addr);
			sub.setsockopt(ZMQ_SUBSCRIBE, "", 0);
		}
		catch (const std::exception& e)
		{
			std::clog << "create_sub connect error: addr: " << addr << std::endl;
		}
	};
}

//---------------------------------------------------------------------------

std::string fmt(const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	char* buf = nullptr;
	int bufsz = vasprintf(&buf, fmt, va);
	va_end(va);

	std::string s;
	if (buf && bufsz > 0)
		s.assign(buf, bufsz);
	free(buf);
	return s;
}

zmq::message_t recv(zmq::socket_t& s, int flags)
{
	zmq::message_t msg;
	s.recv(&msg, flags);

	return msg;
}

zmq::message_t to_msg(const string& str)
{
	return zmq::message_t(str.c_str(), str.size());
}

std::string to_string(const zmq::message_t& msg)
{
	return std::string(
				reinterpret_cast<const char *>(msg.data()),
				msg.size());
}
