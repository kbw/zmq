#pragma once

class IFilter
{
public:
	virtual	~IFilter() = 0;
	virtual	string handle(const string& in) = 0;
};

IFilter* create(const string& id);
